import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import {
  DashboardComponent,
  UsersListComponent,
  UserFormComponent,
  DashboardRoutingModule,
  InputFieldModule,
  DirectivesModule,
  ModalModule,
  NotificationModule,
  SelectFieldModule,
  LoaderModule
} from './index';

const COMPONENTS = [
  DashboardComponent,
  UsersListComponent,
  UserFormComponent
];

const MODULES = [
  CommonModule,
  ReactiveFormsModule,
  ModalModule,
  NotificationModule,
  DirectivesModule,
  DashboardRoutingModule,
  InputFieldModule,
  SelectFieldModule,
  LoaderModule
];

@NgModule({
  declarations: COMPONENTS,
  imports: MODULES
})
export class DashboardModule {
}
