import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { UserFormComponent } from '../user-form/user-form.component';
import { ModalService } from '../../shared/components/modal/services/modal.service';
import { UserListItem } from '../../shared/interfaces/UserListItem';
import { DatabaseService } from '../../shared/services/database.service';
import { UserModalData } from '../../shared/interfaces/UserModalData';
import { UserModalAction } from '../../shared/enums/UserModalAction';
import { NotificationService } from '../../shared/components/notification/services/notification.service';
import { MESSAGES } from '../../shared/constants/messages';

@Component({
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  public loading = false;
  public usersList: UserListItem[] = [];

  constructor(private modalService: ModalService,
              private notificationService: NotificationService,
              private viewContainerRef: ViewContainerRef,
              private databaseService: DatabaseService) {
  }

  ngOnInit(): void {
    this.getUsersList();
  }

  getUsersList() {
    this.loading = true;
    this.databaseService.getUsersList()
      .subscribe({
        next: res => {
          this.usersList = res;
          this.loading = false;
        },
        error: err => {
          this.loading = false;
        }
      });
  }

  saveUser(user: UserListItem) {
    this.loading = true;
    this.databaseService.saveUser(user)
      .subscribe({
        next: res => {
          this.usersList.push(res);
          this.loading = false;
          const text = MESSAGES.USER.CREATE.SUCCESS;
          this.notificationService.showSuccess(this.viewContainerRef, text);
        },
        error: err => {
          this.loading = false;
          this.notificationService.showError(this.viewContainerRef, err);
        }
      });
  }

  updateUser(user: UserListItem) {
    this.loading = true;
    this.databaseService.updateUser(user)
      .subscribe({
        next: res => {
          this.getUsersList();
          const text = MESSAGES.USER.UPDATE.SUCCESS;
          this.notificationService.showSuccess(this.viewContainerRef, text);
        },
        error: err => {
          this.loading = false;
          this.notificationService.showError(this.viewContainerRef, err);
        }
      });
  }

  deleteUser(user: UserListItem) {
    this.loading = true;
    this.databaseService.deleteUser(user.id)
      .subscribe({
        next: res => {
          this.usersList = this.usersList.filter(item => item.id !== user.id);
          this.loading = false;
          const text = MESSAGES.USER.DELETE.SUCCESS;
          this.notificationService.showSuccess(this.viewContainerRef, text);
        },
        error: err => {
          this.loading = false;
          const text = MESSAGES.USER.DELETE.ERROR;
          this.notificationService.showError(this.viewContainerRef, text);
        }
      });
  }

  openCreateUserModal() {
    const title = 'Create user';
    this.modalService.open(UserFormComponent, this.viewContainerRef, title)
      ?.subscribe((data: UserModalData) => {
        this.handleUserModalClose(data);
      })
  }

  openEditUserModal(user: UserListItem) {
    const title = `${user.firstName} ${user.lastName}`;
    this.modalService.open(UserFormComponent, this.viewContainerRef, title, {
      userData: user
    })
      ?.subscribe((data: UserModalData) => {
        this.handleUserModalClose(data);
      })
  }

  handleUserModalClose(data: UserModalData) {
    const {SAVE, UPDATE, DELETE} = UserModalAction;
    const {user, action} = data;

    if (action === SAVE) {
      this.saveUser(user);
    } else if (action === UPDATE) {
      this.updateUser(user);
    } else if (action === DELETE) {
      this.deleteUser(user);
    }
  }

}
