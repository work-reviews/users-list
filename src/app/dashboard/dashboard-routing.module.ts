import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ROUTES } from '../shared/constants/routes';
import { DashboardComponent, UsersListComponent } from './index';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: ROUTES.DASHBOARD.USERS_LIST,
        component: UsersListComponent
      },
      {
        path: '**',
        redirectTo: ROUTES.DASHBOARD.USERS_LIST
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
