import { Component, Input, OnInit } from '@angular/core';
import { FormParameter } from '../../shared/enums/FormParameter';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { globalValidation } from '../../shared/constants/form-validation/global-validation';
import { VALIDATION_PATTERN } from '../../shared/constants/form-validation/validation-patterns';
import { UserType } from '../../shared/enums/UserType';
import { ModalService } from '../../shared/components/modal/services/modal.service';
import { UserListItem } from '../../shared/interfaces/UserListItem';
import { UserModalAction } from '../../shared/enums/UserModalAction';
import { UserModalData } from '../../shared/interfaces/UserModalData';

const NEW_USER_DATA: UserListItem = {
  userName: '',
  firstName: '',
  lastName: '',
  userType: UserType.Admin,
  email: ''
};

@Component({
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  @Input() public userData: UserListItem;
  protected readonly FormParameter = FormParameter;
  public form: FormGroup;
  public userType = UserType;
  public isEdit = false;

  constructor(private formBuilder: FormBuilder,
              private modalService: ModalService,) {
  }

  ngOnInit(): void {
    if (this.userData) {
      this.isEdit = true;
    } else {
      this.userData = NEW_USER_DATA;
    }

    this.createForm();
  }

  createForm(): void {
    const params = globalValidation().params;
    const {userName, firstName, lastName, userType, email} = this.userData;

    this.form = this.formBuilder.group({
      [FormParameter.USER_NAME]: [userName, [
        Validators.required,
        Validators.minLength(params.userName.minlength!),
        Validators.maxLength(params.userName.maxlength!)
      ]],
      [FormParameter.FIRST_NAME]: [firstName, [
        Validators.required,
        Validators.minLength(params.firstName.minlength!),
        Validators.maxLength(params.firstName.maxlength!)
      ]],
      [FormParameter.LAST_NAME]: [lastName, [
        Validators.required,
        Validators.minLength(params.lastName.minlength!),
        Validators.maxLength(params.lastName.maxlength!)
      ]],
      [FormParameter.USER_TYPE]: [userType, [
        Validators.required
      ]],
      [FormParameter.PASSWORD]: [null, [
        Validators.required,
        Validators.minLength(params.password.minlength!),
        Validators.maxLength(params.password.maxlength!),
        Validators.pattern(VALIDATION_PATTERN.PASSWORD)
      ]],
      [FormParameter.PASSWORD_CONFIRM]: [null, [
        Validators.required
      ]],
      [FormParameter.EMAIL]: [email, [
        Validators.required,
        Validators.maxLength(params.email.maxlength!),
        Validators.pattern(VALIDATION_PATTERN.EMAIL)
      ]],
    }, {validators: this.checkPasswords});
  }

  checkPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    const pass = group.get(this.FormParameter.PASSWORD)!.value;
    const confirmPass = group.get(this.FormParameter.PASSWORD_CONFIRM)!.value;
    return pass === confirmPass ? null : {notSame: true};
  }

  submit() {
    if (this.form.valid) {
      let user = this.form.getRawValue();
      let action = UserModalAction.SAVE;

      if (this.isEdit) {
        action = UserModalAction.UPDATE;

        user = {
          ...this.userData,
          ...user
        };
      }

      this.modalService.close({
        action,
        user
      } as UserModalData);
    }
  }

  deleteUser() {
    this.modalService.close({
      action: UserModalAction.DELETE,
      user: this.userData
    } as UserModalData);
  }

}
