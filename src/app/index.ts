export * from './app.component';
export * from './pages/page-forbidden/page-forbidden.component';
export * from './pages/page-not-found/page-not-found.component'

export * from './app-routing.module';
