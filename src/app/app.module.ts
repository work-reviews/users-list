import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {
  AppComponent,
  AppRoutingModule,
  PageForbiddenComponent,
  PageNotFoundComponent
} from './index';

const COMPONENTS = [
  AppComponent,
  PageForbiddenComponent,
  PageNotFoundComponent
];

const MODULES = [
  BrowserModule,
  AppRoutingModule
];

@NgModule({
  declarations: COMPONENTS,
  imports: MODULES,
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
