import { AbstractControl, ValidationErrors } from '@angular/forms';
import { VALIDATION_MESSAGE } from '../constants/form-validation/validation-messages';
import { FormParameter } from '../enums/FormParameter';
import { ValidationParams } from '../interfaces/form-validation/ValidationParams';
import { FormValidation } from '../interfaces/form-validation/FormValidation';

const removeSpaces = (control: AbstractControl) => {
  if (control && control.value && !control.value.replace(/\s/g, '').length) {
    control.setValue('');
  }
  return null;
}

const getValidationMessages = (validationParams: ValidationParams) => {
  let formMessages = {};

  Object.entries(validationParams).forEach(([key, value]) => {

    let fieldMessages = {};

    formMessages = {
      ...formMessages,
      [key as FormParameter]: {
        ...fieldMessages,
        ...value.required ? VALIDATION_MESSAGE.REQUIRED(value.name) : null,
        ...value.min ? VALIDATION_MESSAGE.MIN(value.name, value.min) : null,
        ...value.max ? VALIDATION_MESSAGE.MAX(value.name, value.max) : null,
        ...value.minlength ? VALIDATION_MESSAGE.MINLENGTH(value.name, value.minlength) : null,
        ...value.maxlength ? VALIDATION_MESSAGE.MAXLENGTH(value.name, value.maxlength) : null,
        ...value.patternEmail ? VALIDATION_MESSAGE.PATTERN_EMAIL : null,
        ...value.patternPassword ? VALIDATION_MESSAGE.PATTERN_PASSWORD : null
      }
    };
  })

  return formMessages;
}

const getFormErrors = (validationParams: ValidationParams): ValidationErrors => {
  let formErrors: ValidationErrors = {};

  Object.entries(validationParams).forEach(([key, value]) => {
    formErrors = {
      ...formErrors,
      [key as FormParameter]: ''
    };
  })

  return formErrors;
}

const getValidation = (validationParams: ValidationParams): FormValidation => {
  return <FormValidation>{
    params: validationParams,
    errors: getFormErrors(validationParams),
    messages: getValidationMessages(validationParams)
  };
};

export const FORM_VALIDATION = {
  removeSpaces,
  getValidation
}
