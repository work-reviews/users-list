export enum FormParameter {
  USER_NAME = 'userName',
  FIRST_NAME = 'firstName',
  LAST_NAME = 'lastName',
  USER_TYPE = 'userType',
  EMAIL = 'email',
  PASSWORD = 'password',
  PASSWORD_CONFIRM = 'passwordConfirm'
}
