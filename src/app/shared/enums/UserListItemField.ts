export enum UserListItemField {
  USER_NAME = 'User name',
  FIRST_NAME = 'First name',
  LAST_NAME = 'Last name',
  EMAIL = 'Email',
  TYPE = 'Type',
}
