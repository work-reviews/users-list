export enum UserModalAction {
  SAVE = 'save',
  UPDATE = 'update',
  DELETE = 'delete'
}
