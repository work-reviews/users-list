import {
  ApplicationRef,
  ComponentRef,
  Inject,
  Injectable,
  Injector,
  TemplateRef, Type,
  ViewContainerRef
} from '@angular/core';
import { ModalComponent } from '../modal.component';
import { DOCUMENT } from '@angular/common';
import { Subject } from 'rxjs';

export type Content<T> = string | TemplateRef<T> | Type<T>;

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  private modalEvent?: Subject<any>;
  private modal: ComponentRef<any>;

  constructor(private injector: Injector,
              private applicationRef: ApplicationRef,
              @Inject(DOCUMENT) private document: Document) {
  }

  open(content: Type<any>, vcr: ViewContainerRef, title?: string, data?: { [key: string]: any }) {
    const modalContent = this.resolveContent(content, vcr, data);
    this.modal = vcr.createComponent(ModalComponent, {
      injector: this.injector,
      projectableNodes: modalContent
    });

    this.modal.instance.closeEvent.subscribe(() => this.close());
    this.modal.instance.title = title;

    this.modal.hostView.detectChanges();

    this.modalEvent = new Subject();
    return this.modalEvent?.asObservable();
  }

  close(data?: any) {
    if (data) {
      this.modalEvent?.next(data);
    }
    this.modalEvent?.complete();
    this.modal.destroy();
  }

  private resolveContent<T>(content: Content<T>, vcr: ViewContainerRef, data?: { [key: string]: any }): any[][] | Text[][] {
    if (typeof content === 'string') {
      const element = this.document.createTextNode(content);
      return [[element]];
    }

    if (content instanceof TemplateRef) {
      const viewRef = content.createEmbeddedView(null as any);
      this.applicationRef.attachView(viewRef);
      return [viewRef.rootNodes];
    }

    if (typeof (content) === 'function') {
      const component = vcr.createComponent(content);
      if (data) {
        const key = Object.keys(data)[0]
        const value = Object.values(data)[0];
        component.setInput(key, value);
      }

      return [[component.location.nativeElement]];
    }

    return [[]];
  }
}
