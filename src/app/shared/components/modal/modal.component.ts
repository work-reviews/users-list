import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output
} from '@angular/core';

@Component({
  selector: 'modal-component',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
  @Input() public title: string;
  @Output() public closeEvent = new EventEmitter();

  constructor(private elementRef: ElementRef) {
  }

  private destroyElement() {
    this.elementRef.nativeElement.remove();
  }

  close(): void {
    this.destroyElement();
    this.closeEvent.emit();
  }
}
