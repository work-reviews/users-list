import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  ModalComponent
} from './index';

const COMPONENTS = [
  ModalComponent
];

const MODULES = [
  CommonModule
];

@NgModule({
  imports: MODULES,
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ModalModule {
}
