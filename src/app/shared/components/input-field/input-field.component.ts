import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { globalValidation } from '../../constants/form-validation/global-validation';
import { FormParameter } from '../../enums/FormParameter';

@Component({
  selector: 'input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss'],
})
export class InputFieldComponent implements OnInit {
  @Input() public form: FormGroup;
  @Input() public controlName: FormParameter;
  @Input() public control: FormControl;
  @Input() public label: string = 'Label';
  @Input() public value?: string;
  @Input() public placeholder: string;
  @Input() public type: string = 'text';
  @Input() public autocomplete: string = 'off';
  @Input() public errorMessage: string | undefined;
  @Input() public minlength: number;
  @Input() public maxlength: number;
  @Input() public min: number;
  @Input() public max: number;
  public validation = globalValidation();
  public required: boolean = false;

  constructor() {
  }

  ngOnInit() {
    this.control = this.form.get(this.controlName) as FormControl;
    this.required = this.control.hasValidator(Validators.required);
  }
}
