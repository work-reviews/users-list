import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import {
  InputFieldComponent,
  DirectivesModule
} from './index';

const COMPONENTS = [
  InputFieldComponent
];

const MODULES = [
  CommonModule,
  ReactiveFormsModule,
  DirectivesModule
];

@NgModule({
  imports: MODULES,
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class InputFieldModule {
}
