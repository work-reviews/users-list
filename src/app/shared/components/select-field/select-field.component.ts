import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { FormParameter } from '../../enums/FormParameter';

@Component({
  selector: 'select-field',
  templateUrl: './select-field.component.html',
  styleUrls: ['./select-field.component.scss']
})
export class SelectFieldComponent implements OnInit {
  @Input() public form: FormGroup;
  @Input() public controlName: FormParameter;
  @Input() public control: FormControl;
  @Input() public label: string = 'Label';
  @Input() public options: {[key: string]: string};

  constructor() {
  }

  ngOnInit(): void {
    this.control = this.form.get(this.controlName) as FormControl;
  }

}
