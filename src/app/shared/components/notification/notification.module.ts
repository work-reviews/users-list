import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  NotificationComponent
} from './index';

const COMPONENTS = [
  NotificationComponent
];

const MODULES = [
  CommonModule
];

@NgModule({
  imports: MODULES,
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class NotificationModule {
}
