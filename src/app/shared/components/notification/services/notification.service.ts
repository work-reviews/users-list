import { ComponentRef, Injectable, ViewContainerRef } from '@angular/core';
import { NotificationComponent } from '../notification.component';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private notification: ComponentRef<any>;
  private notificationTimeout: any;

  constructor() {
  }

  showSuccess(vcr: ViewContainerRef, text: string, title?: string, time?: number) {
    this.open('success', vcr, text, title);
  }

  showError(vcr: ViewContainerRef, text: string, title?: string, time?: number) {
    this.open('error', vcr, text, title);
  }

  private open(type: string, vcr: ViewContainerRef, text: string, title?: string, time?: number) {
    this.notification = vcr.createComponent(NotificationComponent);

    this.notification.instance.closeEvent.subscribe(() => this.close());
    this.notification.instance.title = title;
    this.notification.instance.text = text;

    if (type === 'success') {
      this.notification.instance.isSuccess = true;
    } else if (type === 'error') {
      this.notification.instance.isError = true;
    }

    this.notification.hostView.detectChanges();

    clearTimeout(this.notificationTimeout);
    this.notificationTimeout = setTimeout(() => {
      this.close();
    }, time || 2000);
  }

  close() {
    this.notification.destroy();
  }

}
