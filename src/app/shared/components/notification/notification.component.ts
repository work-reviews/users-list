import { Component, ElementRef, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent {
  @Input() public title: string;
  @Input() public text: string;
  @Input() public isSuccess: boolean = false;
  @Input() public isError: boolean = false;
  @Output() public closeEvent = new EventEmitter();

  constructor(private elementRef: ElementRef) {
  }

  private destroyElement() {
    this.elementRef.nativeElement.remove();
  }

  close(): void {
    this.destroyElement();
    this.closeEvent.emit();
  }
}
