import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  LoaderComponent
} from './index';

const COMPONENTS = [
  LoaderComponent
];

const MODULES = [
  CommonModule
];

@NgModule({
  imports: MODULES,
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class LoaderModule {
}
