export const MESSAGES = {
  USER: {
    CREATE: {
      SUCCESS: 'User was created',
      ERROR: 'Failed to create',
      ERROR_NOT_UNIQUE: 'Failed! Not a unique username'
    },
    UPDATE: {
      SUCCESS: 'User was updated',
      ERROR: 'Failed to update!',
      ERROR_NOT_UNIQUE: 'Failed! Not a unique username'
    },
    DELETE: {
      SUCCESS: 'User was deleted',
      ERROR: 'Failed to delete'
    }
  }
}
