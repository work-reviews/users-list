export const ROOT_ROUTES = {
  DASHBOARD: 'dashboard',
  PAGE_404: 'page-not-found',
  PAGE_403: 'page-forbidden'
};

export const ROUTES = {
  DASHBOARD: {
    USERS_LIST: 'users-list'
  }
};

const getDashboardRoute = (route: string) => {
  return `${ROOT_ROUTES.DASHBOARD}/${route}`;
};

export const DASHBOARD_ROUTES = {
  USERS_LIST: getDashboardRoute(ROUTES.DASHBOARD.USERS_LIST)
};
