import { FormParameter } from '../../enums/FormParameter';
import { ValidationParams } from '../../interfaces/form-validation/ValidationParams';
import { FormValidation } from '../../interfaces/form-validation/FormValidation';
import { FORM_VALIDATION } from '../../utils/form-validatiors';

const {
  EMAIL,
  PASSWORD,
  PASSWORD_CONFIRM,
  USER_NAME,
  FIRST_NAME,
  LAST_NAME,
  USER_TYPE
} = FormParameter;

export const globalValidationParams: ValidationParams = {
  [EMAIL]: {
    name: 'Email',
    maxlength: 50,
    required: true,
    patternEmail: true
  },
  [PASSWORD]: {
    name: 'Password',
    minlength: 8,
    maxlength: 24,
    required: true,
    patternPassword: true
  },
  [PASSWORD_CONFIRM]: {
    name: 'Confirm password'
  },
  [USER_NAME]: {
    name: 'User name',
    required: true,
    minlength: 3,
    maxlength: 50
  },
  [FIRST_NAME]: {
    name: 'First name',
    required: true,
    minlength: 3,
    maxlength: 50
  },
  [LAST_NAME]: {
    name: 'Last name',
    required: true,
    minlength: 3,
    maxlength: 50
  },
  [USER_TYPE]: {
    name: 'User type',
    required: true
  }
};

export const globalValidation = (): FormValidation => FORM_VALIDATION.getValidation(globalValidationParams);
