export const VALIDATION_PATTERN = {
  EMAIL: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,10}$/,
  PASSWORD: /^(?=.*?[A-Za-z])(?=.*?[0-9]).{8,}$/
};
