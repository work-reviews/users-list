export const VALIDATION_MESSAGE = {
  REQUIRED: (name: string) => {
    return {
      required: `${name} is required`,
    };
  },
  MIN: (name: string, min: number) => {
    return {
      min: `${name} minimum is ${min}`,
    };
  },
  MAX: (name: string, max: number) => {
    return {
      max: `${name} maximum is ${max}`,
    };
  },
  MINLENGTH: (name: string, minlength: number) => {
    return {
      minlength: `${name} minimum length is ${minlength} symbols`,
    };
  },
  MAXLENGTH: (name: string, maxlength: number) => {
    return {
      maxlength: `${name} maximum length is ${maxlength} symbols`,
    };
  },
  PATTERN_EMAIL: {
    pattern: 'Please enter valid email address.',
  },
  PATTERN_PASSWORD: {
    pattern: 'Password must be minimum 8 characters and contain at least one number and one letter',
  }
};
