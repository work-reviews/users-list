import { UserModalAction } from '../enums/UserModalAction';
import { UserListItem } from './UserListItem';

export interface UserModalData {
  action: UserModalAction;
  user: UserListItem
}
