import { FormParameter } from '../../enums/FormParameter';

export type ValidationErrors = {
  [key in FormParameter]: string;
};
