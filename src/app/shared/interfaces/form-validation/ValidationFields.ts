import { FormParameter } from '../../enums/FormParameter';

export type ValidationFields = {
  [key in FormParameter]: any;
};
