import { FormParameter } from '../../enums/FormParameter';

export type ValidationParams = {
  [key in FormParameter]: {
    name: string;
    min?: number;
    max?: number;
    minlength?: number;
    maxlength?: number;
    required?: boolean;
    patternPassword?: boolean;
    patternEmail?: boolean;
  };
};
