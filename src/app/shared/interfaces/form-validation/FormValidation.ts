import { ValidationParams } from './ValidationParams';
import { ValidationErrors } from './ValidationErrors';

export interface FormValidation {
  params: ValidationParams;
  errors: ValidationErrors;
  messages: any;
}
