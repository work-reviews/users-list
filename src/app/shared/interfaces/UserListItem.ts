import { UserType } from '../enums/UserType';

export interface UserListItem {
  id?: number;
  userName: string;
  firstName: string;
  lastName: string;
  email: string;
  userType: UserType
}
