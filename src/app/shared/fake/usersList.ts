import { UserListItem } from '../interfaces/UserListItem';
import { UserType } from '../enums/UserType';

export const USERS_LIST: UserListItem[] = [
  {
    userName: 'mperry1992',
    firstName: 'Matthew',
    lastName: 'Perry',
    email: 'Perry',
    userType: UserType.Admin
  },
  {
    userName: 'mperry1992',
    firstName: 'Matthew',
    lastName: 'Perry',
    email: 'Perry',
    userType: UserType.Admin
  },
  {
    userName: 'mperry1992',
    firstName: 'Matthew',
    lastName: 'Perry',
    email: 'Perry',
    userType: UserType.Admin
  },
  {
    userName: 'mperry1992',
    firstName: 'Matthew',
    lastName: 'Perry',
    email: 'Perry',
    userType: UserType.Admin
  },
  {
    userName: 'mperry1992',
    firstName: 'Matthew',
    lastName: 'Perry',
    email: 'Perry',
    userType: UserType.Admin
  },
  {
    userName: 'mperry1992',
    firstName: 'Matthew',
    lastName: 'Perry',
    email: 'Perry',
    userType: UserType.Admin
  },
  {
    userName: 'mperry1992',
    firstName: 'Matthew',
    lastName: 'Perry',
    email: 'Perry',
    userType: UserType.Admin
  },
  {
    userName: 'mperry1992',
    firstName: 'Matthew',
    lastName: 'Perry',
    email: 'Perry',
    userType: UserType.Admin
  },
  {
    userName: 'mperry1992',
    firstName: 'Matthew',
    lastName: 'Perry',
    email: 'Perry',
    userType: UserType.Admin
  },
  {
    userName: 'mperry1992',
    firstName: 'Matthew',
    lastName: 'Perry',
    email: 'Perry',
    userType: UserType.Admin
  }
]
