import { Directive, HostListener, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Directive({
  selector: '[formSubmit]'
})
export class FormSubmitDirective {
  @Input() validationControl: FormGroup;

  constructor() {
  }

  @HostListener('click', ['$event'])
  handleClickEvent() {
    this.markAsTouched(this.validationControl);
  }

  private markAsTouched(formGroup: FormGroup): void {
    formGroup.markAsTouched();
    formGroup.updateValueAndValidity();
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched();
        control.updateValueAndValidity({onlySelf: false, emitEvent: true});
      } else if (control instanceof FormGroup) {
        this.markAsTouched(control);
      }
    });
  }
}
