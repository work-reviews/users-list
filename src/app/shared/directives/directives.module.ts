import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormValidateDirective } from './form-validate.directive';
import { ReactiveFormsModule } from '@angular/forms';
import { FormSubmitDirective } from './form-submit.directive';

const DIRECTIVES = [
  FormValidateDirective,
  FormSubmitDirective
];

const MODULES = [
  CommonModule,
  ReactiveFormsModule
];

@NgModule({
  imports: MODULES,
  declarations: DIRECTIVES,
  exports: DIRECTIVES
})
export class DirectivesModule {
}
