import { Directive, ElementRef, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { FormParameter } from '../enums/FormParameter';
import { FormValidation } from '../interfaces/form-validation/FormValidation';

@Directive({
  selector: '[formValidate]'
})
export class FormValidateDirective implements OnInit, OnDestroy {
  @Input('validation') validation: FormValidation;
  @Input('controlName') controlName: FormParameter;
  private subscriptions: Subscription[] = [];

  constructor(private elementRef: ElementRef,
              private field: NgControl) {
  }

  ngOnInit(): void {
    this.startSubscriptions();
    this.subscribeStatusChanges();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  startSubscriptions() {
    this.subscriptions.push(
      this.subscribeStatusChanges()
    )
  }

  subscribeStatusChanges() {
    const control = this.field.control;
    return control!.statusChanges.subscribe(status => {
        if (status == 'INVALID') {
          this.showError();
        } else {
          this.hideError();
        }
      }
    )
  }

  private hideError(): void {
    if (this.validation) {
      const {errors, messages} = this.validation;
      const name = this.controlName;
      errors[name] = '';
    }
  }

  private showError(): void {
    this.hideError();
    const {errors, messages} = this.validation;
    const name = this.controlName;
    const message = messages[name];
    for (const key in this.field.errors) {
      errors[name] = message[key] || '';
    }
  }

  @HostListener('blur', ['$event'])
  handleBlurEvent() {
    const value = this.field.value;
    if (value === null || value === '') {
      if (this.field.errors) this.showError();
      else this.hideError();
    }
  }
}
