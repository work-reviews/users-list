import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
  }

  saveData(key: string, data: any): void {
    if (isPlatformBrowser(this.platformId)) {
      try {
        localStorage.setItem(key, JSON.stringify(data));
      } catch (e) {
        console.log(e);
      }
    }
  }

  getData(key: string) {
    if (isPlatformBrowser(this.platformId)) {
      try {
        const data = localStorage.getItem(key);
        if (data) {
          return JSON.parse(data);
        }
      } catch (e) {
        console.log(e);
      }

    }
    return null;
  }

  removeData(key: string) {
    if (isPlatformBrowser(this.platformId)) {
      try {
        localStorage.removeItem(key);
      } catch (e) {
        console.log(e);
      }
    }
  }

}
