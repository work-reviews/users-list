import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { APP_NAME } from '../constants/constants';
import { UserListItem } from '../interfaces/UserListItem';
import { delay, map, Observable, of, throwError } from 'rxjs';
import { MESSAGES } from '../constants/messages';

const KEY = APP_NAME + '_DB';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  constructor(private storage: StorageService) {
  }

  saveUser(user: UserListItem) {
    return this.getUsersList().pipe(map(oldData => {
      const isUniqueUserName = this.isUniqueUserName(user, oldData)
      if (isUniqueUserName) {
        let newData: UserListItem[] = [];
        const userData = {
          ...user,
          id: Date.now()
        };

        if (oldData?.length) {
          newData = [
            ...oldData,
            userData
          ];
        } else {
          newData.push(userData);
        }

        this.storage.saveData(KEY, newData);
        return userData;
      } else {
        throw new Error(MESSAGES.USER.CREATE.ERROR_NOT_UNIQUE);
      }
    })).pipe(delay(1000));
  }

  updateUser(user: UserListItem) {
    return this.getUsersList().pipe(map(oldData => {
      const isUniqueUserName = this.isUniqueUserName(user, oldData)
      if (isUniqueUserName) {
        let newData: UserListItem[] = [];
        if (oldData?.length) {
          newData = oldData.map(item => {
            return user.id === item.id ? user : item
          });
        } else {
          newData.push(user);
        }

        this.storage.saveData(KEY, newData);
        return user;
      } else {
        throw new Error(MESSAGES.USER.UPDATE.ERROR_NOT_UNIQUE);
      }
    })).pipe(delay(1000));
  }

  deleteUser(id: UserListItem['id']) {
    return this.getUsersList().pipe(map(oldData => {
      let newData: UserListItem[] = [];
      if (oldData?.length) {
        newData = oldData.filter(item => id !== item.id);
      }

      this.storage.saveData(KEY, newData);
      return id;
    })).pipe(delay(1000));
  }

  private isUniqueUserName(user: UserListItem, data: UserListItem[]) {
    return !data.find(item => item.userName.trim() === user.userName.trim());
  }

  getUsersList(): Observable<UserListItem[] | []> {
    const data = this.storage.getData(KEY);
    return of(data || []).pipe(delay(500));
  }

  removeUsersList() {
    return this.storage.removeData(KEY);
  }

}
