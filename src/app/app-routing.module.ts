import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ROOT_ROUTES } from './shared/constants/routes';
import { PageForbiddenComponent } from './pages/page-forbidden/page-forbidden.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: ROOT_ROUTES.DASHBOARD,
    pathMatch: 'full',
  },
  {
    path: ROOT_ROUTES.DASHBOARD,
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: ROOT_ROUTES.PAGE_403,
    component: PageForbiddenComponent
  },
  {
    path: ROOT_ROUTES.PAGE_404,
    component: PageNotFoundComponent
  },
  {
    path: '**',
    redirectTo: ROOT_ROUTES.DASHBOARD
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
